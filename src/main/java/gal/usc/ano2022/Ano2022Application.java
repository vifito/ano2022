package gal.usc.ano2022;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ano2022Application {

	public static void main(String[] args) {
		SpringApplication.run(Ano2022Application.class, args);
	}

}
